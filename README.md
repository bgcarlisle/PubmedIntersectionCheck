**NOTE: This repo is no longer being maintained, as its functions have been folded into the `pubmedtk` package: https://github.com/bgcarlisle/pubmedtk**

# PubmedIntersectionCheck

This R script takes a list of Pubmed ID's and checks in batches whether they would appear in the results for another Pubmed search

## Requirements

This *R* script uses the `tidyverse`, `xml2` and `httr` libraries.

## How to use

First, [make an NCBI account](https://www.ncbi.nlm.nih.gov/account/register/) (if you don't have one) and [generate an API key](https://www.ncbi.nlm.nih.gov/account/settings/).

Copy your API key into a file named `api_key.txt` in the project folder.

Then, copy a CSV with a column named `pmid` that contains a list of Pubmed ID's to be checked into the project folder. The *R* script `checker.R` will look for this script at `pmids.csv`, but you can change this on line 10 if you really feel like it.

Edit the file `query.txt` to contain the Pubmed search you are interested in. By default, it is [a very long search string that returns animal studies from Hooijmans et al](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3104815/).

When you run the *R* script `checker.R`, it will write a new CSV with two columns: `pmid` and `found` in batches of a size specified by the batchsize variable on line 26. The `pmid` column will contain all the PMIDs from the original CSV, and `found` will be 0 or 1. 1 indicates that the PMID is not in the set of PMIDs returned by the Pubmed search in `query.txt`, 0 indicates that it is not.
